//
//  ViewController.m
//  HelloUIStepper
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/17.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeStepper:(UIStepper *)sender {
    
    _showValue.text=[NSString stringWithFormat:@"%.0f",sender.value];
    
}
@end
